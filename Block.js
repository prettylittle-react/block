import React from 'react';
import PropTypes from 'prop-types';

import Container from 'container';
//import Img from 'img';
import {getModifiers} from 'libs/component';

import InViewObserver from 'inviewobserver';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import './Block.scss';

/**
 * Block
 * @description [Description]
 * @example
  <div id="Block"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Block, {
    	title : 'Example Block'
    }), document.getElementById("Block"));
  </script>
 */
class Block extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'block';
	}

	onInView = () => {
		this.component.classList.add('active');
	};

	renderBefore() {
		const {image} = this.props;

		if (image) {
			// TODO: fix object fit for IE11
			// const style = {backgroundImage: "url(" + image.src + ")"};
			// return <div style={style} className={`${this.baseClass}__image`} />;
			return <Img {...image} className={`${this.baseClass}__image`} />;
		}

		return null;
	}

	renderAfter() {
		return null;
	}

	render() {
		const {id, width, children, classes, theme, modifiers, vPadding} = this.props;

		if (children === null) {
			return null;
		}

		const atts = {
			id,
			className: getModifiers(this.baseClass, [modifiers, `pad-${vPadding}`])
		};

		if (classes) {
			atts.className += ` ${classes}`;
		}

		if (theme) {
			atts['data-theme'] = theme;
		}

		return (
			<InViewObserver onInView={this.onInView} isActive={true}>
				<div {...atts} ref={component => (this.component = component)}>
					{this.renderBefore()}
					<Container size={width}>{children}</Container>
					{this.renderAfter()}
				</div>
			</InViewObserver>
		);
	}
}

Block.defaultProps = {
	image: null,
	theme: null,
	classes: null,
	modifiers: null,
	vPadding: 'default',
	width: 'default',
	id: null,
	children: null
};

Block.propTypes = {
	image: PropTypes.object,
	theme: PropTypes.string,
	classes: PropTypes.string,
	modifiers: PropTypes.string,
	vPadding: PropTypes.string,
	width: PropTypes.string,
	id: PropTypes.string,
	children: PropTypes.node
};

export default Block;
